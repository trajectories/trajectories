Modern smartphones are equipped with an array of powerful sensors that can continuously sense the ambient space. In contrast GPS units, most of these sensors have very modest power requirements, so it is feasible to have them permanently turned on. For example, detecting nearby mobile network base stations, WiFi access points, and Bluetooth devices, or measuring acceleration, magnetic fields, and air pressure can be performed continuously with hardly affecting battery life of mobile devices. So in principle every mobile user is a potential source of continuous geospatial data that can be tapped into. The first goal of this proposed research is the systematic acquisition and processing of geospatial data from such lightweight sensors. Ideally, with everyone voluntarily contributing their sensor readings one could process this data into a humongous amount of fuzzy trajectory data possibly even enriched with other contextual information. Exploiting this huge pool of trajectory data has the potential to help with the solution of grand social challenges e.g. in the fields of environment and disaster management, health, transport and citizen participation. Unfortunately, the methodology to actually mine trajectory data on such a large scale is still in its infancy. Hence the second goal of this proposal is the development of suitable algorithms and data structures for efficient mining huge sets of trajectory data. Our results will also be of great benefit for other projects within the priority programme as we provide a basic toolbox to efficiently acquire and work with trajectory data.


## Selected Publications

<ul class="publications">
    <li class="paper-conference">Seybold, M. P. (2017). Robust Map Matching for Heterogeneous Data via Dominance Decompositions. In N. Chawla &amp; W. Wang, N. Chawla &amp; W. Wang (Eds.), <i>SDM</i> (pp. 813–821). SIAM. Retrieved from http://dblp.uni-trier.de/db/conf/sdm/sdm2017.html#Seybold17 <span class="opt">[<a href="http://dblp.uni-trier.de/db/conf/sdm/sdm2017.html#Seybold17">URL</a> | <a href="https://www.bibsonomy.org/bib/publication/f524158eacb74e5af96c2d86c4c45de9/mobeck">BibTeX</a> | <a href="https://www.bibsonomy.org/publication/f524158eacb74e5af96c2d86c4c45de9/mobeck">BibSonomy</a>]</span></li>
    <li class="paper-conference">Barth, F., Funke, S., &amp; Storandt, S. (2019). Alternative Multicriteria Routes. In <i>ALENEX</i>. SIAM. <span class="opt">[<a href="https://www.bibsonomy.org/bib/publication/6082bd760ad8db94d078339421a9e655/mobeck">BibTeX</a> | <a href="https://www.bibsonomy.org/publication/6082bd760ad8db94d078339421a9e655/mobeck">BibSonomy</a>]</span></li>
    <li class="paper-conference">Funke, S., Rupp, T., Nusser, A., &amp; Storandt, S. (2019). PATHFINDER: Storage and Indexing of Massive Trajectory Sets. In <i>Proceedings of the 16th International Symposium on Spatial and Temporal Databases</i> (pp. 90–99). Vienna, Austria: ACM. https://doi.org/10.1145/3340964.3340978 <span class="opt">[DOI:<a href="https://doi.org/10.1145/3340964.3340978">10.1145/3340964.3340978</a> | <a href="http://doi.acm.org/10.1145/3340964.3340978">URL</a> | <a href="https://www.bibsonomy.org/bib/publication/b375ca985b97be8a764ed0ea75d4f02e/ruppts">BibTeX</a> | <a href="https://www.bibsonomy.org/publication/b375ca985b97be8a764ed0ea75d4f02e/ruppts">BibSonomy</a>]</span></li>
</ul>


## Data Sets and Benchmarks

We extracted raw trajectory data as well as the German road and path network  from OpenStreetMap and map matched the trajectories to paths in the network using our novel map matching approach.
The trajectory set contains about 372,000 trajectories consisting of a total of 350 million data points. 
The network consists of about 60 million nodes and 120 million edges. Further details are provided in the enclosed README file.
[Download Data (compressed 5.4GB, uncompressed 19 GB)](https://www.fmi.uni-stuttgart.de/files/alg/data/SPP/trajectory_data.tar.gz )

Further openly available trajectory sets:

* [T-Drive trajectory data sample (10357 trajectories, 15 million data points)](https://www.microsoft.com/en-us/research/publication/t-drive-trajectory-data-sample/)
* [GeoLife GPS Trajectories  (17,621 trajectories )](https://www.microsoft.com/en-us/download/details.aspx?id=52367&from=https%3A%2F%2Fresearch.microsoft.com%2Fen-us%2Fdownloads%2Fb16d359d-d164-469e-9fd4-daa38f2b2e13%2Fdefault.aspx)
* [ECML Taxi Data](https://www.kaggle.com/c/pkdd-15-predict-taxi-service-trajectory-i/data)

 
## Demos

* [OSCAR](http://oscar-web.de/), Spatial search engine for OSM planet data
